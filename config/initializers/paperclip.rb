Paperclip.options[:content_type_mappings] = {
  pdf: "application/pdf"
}
Paperclip.options[:command_path] = '/usr/bin'
